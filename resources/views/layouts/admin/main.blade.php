<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Vito - Responsive Bootstrap 4 Admin Dashboard Template</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="images/favicon.ico" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{ asset('js/chartist/chartist.min.css') }}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{ asset('css/style.css') }}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
      <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
   </head>
   <body>
      <!-- loader Start -->
      <div id="loading">
         <div id="loading-center">
         </div>
      </div>
      <!-- loader END -->
      <!-- Wrapper Start -->
      <div class="wrapper">

         @include('layouts.admin.navigation')

         <!-- Page Content  -->
         <div id="content-page" class="content-page">
            <div class="container-fluid">
               <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" style="background-color:rgba(255, 228, 196, 0)">
                     <li class="breadcrumb-item"><a href="#"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                     <li class="breadcrumb-item active" aria-current="page"></li>
                  </ol>
               </nav>
               
               @include('flash.message')

               @yield('page')
            </div>
         </div>
         
         @include('components.modal')

         {{--  <div class="iq-right-fixed">
            <div class="iq-card mb-0" style="box-shadow: none;">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">Customer Distribution</h4>
                  </div>
                  <div class="iq-card-header-toolbar d-flex align-items-center">
                     <div class="dropdown">
                       <span class="dropdown-toggle" id="dropdownMenuButton-five" data-toggle="dropdown">
                        <a href="#">See All</a>
                        </span>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton-five">
                           <a class="dropdown-item" href="#"><i class="ri-eye-fill mr-2"></i>View</a>
                           <a class="dropdown-item" href="#"><i class="ri-delete-bin-6-fill mr-2"></i>Delete</a>
                           <a class="dropdown-item" href="#"><i class="ri-pencil-fill mr-2"></i>Edit</a>
                           <a class="dropdown-item" href="#"><i class="ri-printer-fill mr-2"></i>Print</a>
                           <a class="dropdown-item" href="#"><i class="ri-file-download-fill mr-2"></i>Download</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div id="chartdiv"></div>
               </div>
            </div>
            <div class="iq-card mb-0" style="box-shadow: none;">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">Project Statistic</h4>
                  </div>
               </div>
                <div class="iq-card-body">
                  <ul class="suggestions-lists m-0 p-0">
                     <li class="d-flex mb-4 align-items-center">
                        <div class="profile-icon iq-bg-danger"><span>G</span></div>
                        <div class="media-support-info ml-3">
                           <h6>Adding Item</h6>
                           <p class="mb-0 fontsize-sm">Development</p>
                        </div>
                        <div class="iq-card-header-toolbar d-flex align-items-center">
                           <div class="dropdown">
                              <span class="dropdown-toggle text-primary" id="dropdownMenuButton41" data-toggle="dropdown">
                              <i class="ri-more-line"></i>
                              </span>
                              <div class="dropdown-menu dropdown-menu-right p-0">
                                 <a class="dropdown-item" href="#"><i class="ri-user-unfollow-line mr-2"></i>Unfollow</a>
                                 <a class="dropdown-item" href="#"><i class="ri-close-circle-line mr-2"></i>Unfriend</a>
                                 <a class="dropdown-item" href="#"><i class="ri-lock-line mr-2"></i>block</a>
                              </div>
                           </div>
                        </div>
                     </li>
                     <li class="d-flex align-items-center">
                        <div class="profile-icon iq-bg-warning"><span>B</span></div>
                        <div class="media-support-info ml-3">
                           <h6>Admin Panel</h6>
                           <p class="mb-0 fontsize-sm">Development</p>
                        </div>
                        <div class="iq-card-header-toolbar d-flex align-items-center">
                           <div class="dropdown">
                              <span class="dropdown-toggle text-primary" id="dropdownMenuButton42" data-toggle="dropdown">
                              <i class="ri-more-line"></i>
                              </span>
                              <div class="dropdown-menu dropdown-menu-right p-0">
                                 <a class="dropdown-item" href="#"><i class="ri-user-unfollow-line mr-2"></i>Unfollow</a>
                                 <a class="dropdown-item" href="#"><i class="ri-close-circle-line mr-2"></i>Unfriend</a>
                                 <a class="dropdown-item" href="#"><i class="ri-lock-line mr-2"></i>block</a>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="iq-card" style="box-shadow: none;">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">Countries</h4>
                  </div>
                  <div class="iq-card-header-toolbar d-flex align-items-center">
                     <div class="dropdown">
                        <span class="dropdown-toggle text-primary" id="dropdownMenuButton6" data-toggle="dropdown">
                        <i class="ri-more-2-fill"></i>
                        </span>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton6">
                           <a class="dropdown-item" href="#"><i class="ri-eye-fill mr-2"></i>View</a>
                           <a class="dropdown-item" href="#"><i class="ri-delete-bin-6-fill mr-2"></i>Delete</a>
                           <a class="dropdown-item" href="#"><i class="ri-pencil-fill mr-2"></i>Edit</a>
                           <a class="dropdown-item" href="#"><i class="ri-printer-fill mr-2"></i>Print</a>
                           <a class="dropdown-item" href="#"><i class="ri-file-download-fill mr-2"></i>Download</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="iq-details">
                           <div class="d-flex align-items-center justify-content-between">
                              <h6 class="title text-dark">United States</h6>
                              <div class="percentage float-right text-primary">95 <span>%</span></div>
                           </div>
                           <div class="iq-progress-bar-linear d-inline-block w-100">
                              <div class="iq-progress-bar">
                                 <span class="bg-primary" data-percent="95"></span>
                              </div>
                           </div>
                        </div>
                        <div class="iq-details mt-3">
                           <div class="d-flex align-items-center justify-content-between">
                              <h6 class="title text-dark">India</h6>
                              <div class="percentage float-right text-warning">75 <span>%</span></div>
                           </div>
                           <div class="iq-progress-bar-linear d-inline-block w-100">
                              <div class="iq-progress-bar">
                                 <span class="bg-warning" data-percent="75"></span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>  --}}

      <!-- Wrapper END -->
      <!-- Footer -->
      <footer class="iq-footer">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-6">
                  <ul class="list-inline mb-0">
                     <li class="list-inline-item"><a href="privacy-policy.html">Privacy Policy</a></li>
                     <li class="list-inline-item"><a href="terms-of-service.html">Terms of</a></li>
                  </ul>
               </div>
               <div class="col-lg-6 text-right">
                  Copyright 2020 <a href="#">Vito</a> All Rights Reserved.
               </div>
            </div>
         </div>
      </footer>

      
      <!-- Footer END -->
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      
      @include('layouts.script')
   </body>
</html>
