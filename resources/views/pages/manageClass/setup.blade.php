@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <div class="card iq-mb-3">
                       <div class="card-body">
                          <h4 class="card-title">Class Name</h4>
                          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                          <a href="{{ Route('class.name') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                       </div>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">Class Arm</h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('class.arm') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">Edit </h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('form.delete') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
@endsection