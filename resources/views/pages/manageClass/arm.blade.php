@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="mb-4">
                <h4>Class Arm</h4>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card p-2">
                        <form action="" method="">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 mb-3">
                                    <label for="validationDefault01">Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="A" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
    </div>
@endsection