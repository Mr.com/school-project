@extends('layouts.admin.main')
@section('page')
   <div class="row">
      <div class="col">
         <div class="row">
            <div class="col-lg-3">
               <div class="card iq-mb-3">
                  <div class="card-body">
                     <h4 class="card-title">Form Title</h4>
                     <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                     <div style="margin-left:auto;float:right">
                        <a href="" class="btn btn-circle btn-outline-info text-center"><i class="las la-pen"></i></a>
                        <a href="" class="btn btn-circle btn-outline-danger text-center"><i class="fa fa-trash"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="card iq-mb-3">
                  <div class="card-body">
                     <h4 class="card-title">Form Title</h4>
                     <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                     <div style="margin-left:auto;float:right">
                        <a href="" class="btn btn-circle btn-outline-info text-center"><i class="las la-pen"></i></a>
                        <a href="" class="btn btn-circle btn-outline-danger text-center"><i class="fa fa-trash"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="card iq-mb-3">
                  <div class="card-body">
                     <h4 class="card-title">Form Title</h4>
                     <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                     <div style="margin-left:auto;float:right">
                        <a href="" class="btn btn-circle btn-outline-info text-center"><i class="las la-pen"></i></a>
                        <a href="" class="btn btn-circle btn-outline-danger text-center"><i class="fa fa-trash"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="card iq-mb-3">
                  <div class="card-body">
                     <h4 class="card-title">Form Title</h4>
                     <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                     <div style="margin-left:auto;float:right">
                        <a href="" class="btn btn-circle btn-outline-info text-center"><i class="las la-pen"></i></a>
                        <a href="" class="btn btn-circle btn-outline-danger text-center"><i class="fa fa-trash"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-3">
               <div class="card iq-mb-3">
                  <div class="card-body">
                     <h4 class="card-title">Form Title</h4>
                     <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
                     <div style="margin-left:auto;float:right">
                        <a href="" class="btn btn-circle btn-outline-info text-center"><i class="las la-pen"></i></a>
                        <a href="" class="btn btn-circle btn-outline-danger text-center"><i class="fa fa-trash"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
@endsection