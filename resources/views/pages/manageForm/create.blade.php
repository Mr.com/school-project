@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            @foreach ($fields as $field)
                            <div class="col-lg-4">
                                <div class="form-group">
                                    @if($field['type'] == 'input')
                                        <label for="">{{ ucwords($field['type']) }}</label>
                                        <input type="text" class="form-control" id="" readonly>
                                    @elseif($field['type'] == 'select')
                                        <label for="">{{ ucwords($field['type']) }}</label>
                                        <select type="password" class="form-control" id="" readonly>
                                            <option hidden>Select Type</option>
                                            <option value="input">Input</option>
                                            <option value="select">Select</option>
                                            <option value="checkbox">CheckBox</option>
                                        </select>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card p-3">
                        <form>
                            <div class="form-group">
                               <label for="">Field Name:</label>
                               <input type="text" class="form-control" id="">
                            </div>
                            <div class="form-group">
                               <label for="">Field Type:</label>
                               <select type="password" class="form-control" id="">
                                    <option hidden>Select Type</option>
                                    <option value="input">Input</option>
                                    <option value="select">Select</option>
                                    <option value="checkbox">CheckBox</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Field Validation:</label>
                                <input type="text" class="form-control" id="">
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                         </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row" style="">
                <div style="margin-left:auto">
                    <button class="btn btn-primary">Finish</button>
                </div>
            </div>
        </div>
    </div>
@endsection