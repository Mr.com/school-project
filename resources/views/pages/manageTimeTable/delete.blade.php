@extends('layouts.admin.main')
@section('page')
   <div class="row">
      <div class="col">
         <div class="iq-comingsoon-info">
            <a href="#">
            <img src="images/logo-full2.png" class="img-fluid w-25" alt="">
            </a>
            <h2 class="mt-4 mb-1">Stay tunned, were launching very soon</h2>
            <p>We are working very hard to give you the best experience possible!</p>
            <ul class="countdown" data-date="Feb 02 2022 20:20:22">
               <li><span data-days>0</span>Days</li>
               <li><span data-hours>0</span>Hours</li>
               <li><span data-minutes>0</span>Minutes</li>
               <li><span data-seconds>0</span>Seconds</li>
            </ul>
         </div>
      </div>
   </div>
@endsection