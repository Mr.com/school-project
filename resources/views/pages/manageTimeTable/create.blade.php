@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            <table class="table table-borderdd">
                                <thead>
                                    <tr>
                                        @foreach($layoutOneColumns as $layoutOneColumn)
                                            <th>{{ $layoutOneColumn->column_id }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($layoutOneRows as $layoutOneRow)
                                    <tr>
                                        @foreach ($layoutOneColumns as $layoutOneColumn)
                                            @foreach($tableSubjects as $tableSubject)
                                                @if (($tableSubject->column_id == $layoutOneColumn->column_id) && 
                                                    ($tableSubject->row_id == $layoutOneRow->row_id))
                                                    <td>{{ $tableSubject->subject }}</td>
                                                @endif 
                                            @endforeach
                                        @endforeach
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card p-3">
                        <form>
                            <div class="form-group">
                               <label for="">Layout</label>
                               <select class="form-control" id="">
                                    <option hidden>Select Type</option>
                                    <option value="layout_one">Layout One</option>
                                    {{--  <option value="select">Select</option>
                                    <option value="checkbox">CheckBox</option>  --}}
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Apply</button>
                         </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row" style="">
                <div style="margin-left:auto">
                    <button class="btn btn-primary">Finish</button>
                </div>
            </div>
        </div>
    </div>
@endsection