@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <div class="card iq-mb-3">
                       <div class="card-body">
                          <h4 class="card-title">New Time Table</h4>
                          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                          <a href="{{ Route('time-table.create') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                       </div>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">Edit Time Table</h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('form.edit') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">Delete Form</h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('form.delete') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">New Layout</h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('form.setup') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
                <div class="col-lg-4 text-center">
                     <div class="card iq-mb-3">
                        <div class="card-body">
                           <h4 class="card-title">Special title treatment</h4>
                           <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                           <a href="{{ Route('form.setup') }}" class="btn dark-icon btn-primary btn-block">Go</a>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
@endsection