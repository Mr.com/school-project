@extends('layouts.admin.main')
@section('page')
    <div class="row">
        <div class="col">
            <div class="row">
               <div class="col-lg-3">
                  <div class="iq-card">
                     <div class="iq-card-body">
                        <div id="morris-donut-chart"></div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-9">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="card iq-mb-3">
                           <div class="row no-gutters">
                              <div class="col-md-4">
                                 <i class="fa fa-user p-4" style="font-size:100px;"></i>
                              </div>
                              <div class="col-md-8">
                                 <div class="card-body">
                                    <h4 class="card-title">Attendance</h4>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in. a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="card iq-mb-3">
                           <div class="row no-gutters">
                              <div class="col-md-4">
                                 <i class="fa fa-user p-4" style="font-size:100px;"></i>
                              </div>
                              <div class="col-md-8">
                                 <div class="card-body">
                                    <h4 class="card-title">Result</h4>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in. a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="card iq-mb-3">
                           <div class="row no-gutters">
                              <div class="col-md-4">
                                 <i class="fa fa-user p-4" style="font-size:100px;"></i>
                              </div>
                              <div class="col-md-8">
                                 <div class="card-body">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in. a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="card iq-mb-3">
                           <div class="row no-gutters">
                              <div class="col-md-4">
                                 <i class="fa fa-user p-4" style="font-size:100px;"></i>
                              </div>
                              <div class="col-md-8">
                                 <div class="card-body">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in. a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        </div>
    </div>
@endsection