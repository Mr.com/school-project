@if($message = Session::get('error'))
<div class="modal show" id="success" >
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row p-4">
                   <div class="col-lg-2">
                      <i class="ion-alert-circled text-danger" style="font-size: 100px"></i>
                   </div>
                   <div class="col-lg-9 p-5">
                      <big style="font-size: larger; font-weight:bold">Message</big>
                      <p style="font-size:large">
                          {{ $message }}  
                      </p>
                   </div>
                </div>
            </div>
        </div>
    </div>
 </div>
 <script>
    $( function(){
        $("#success").modal('show')
        setTimeout( function(){
            $("#success").modal('hide');
        }, 3000)
    })
 </script> 
@elseif($message = Session::get('error'))
<div class="modal show" id="error" >
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row p-4">
                   <div class="col-lg-2">
                      <i class="ion-alert-circled text-danger" style="font-size: 100px"></i>
                   </div>
                   <div class="col-lg-9 p-5">
                      <big style="font-size: larger; font-weight:bold">Message</big>
                      <p style="font-size:large">
                          {{ $message }}  
                      </p>
                   </div>
                </div>
            </div>
        </div>
    </div>
 </div>
 <script>
    $( function(){
        $("#error").modal('show')
        setTimeout( function(){
            $("#error").modal('hide');
        }, 3000)
    })
 </script>  
@endif