<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware'=>['auth']], function(){
    //..--* Dashboard
    Route::controller('\App\Http\Controllers\DashboardController')->group( function(){
        Route::get('dashboard', 'index')->name('dashboard');
    });

    //..--* Form
    Route::controller('\App\Http\Controllers\ManageFormController')
            ->prefix('/form')->name('form.')->group( function()
    {
        Route::get('manage', 'index')->name('manage');
        Route::get('setup', 'show')->name('setup');
        Route::get('create', 'create')->name('create');
        Route::get('edit', 'edit')->name('edit');
        Route::get('delete', 'destroy')->name('delete');
    });

    //..--* Student
    Route::controller('\App\Http\Controllers\ManageStudentController')
            ->prefix('/student')->name('student.')->group( function()
    {
        Route::get('manage', 'index')->name('manage');
        Route::get('setup', 'show')->name('setup');
        Route::post('show', 'show')->name('show');
    });

    //..--* User
    Route::controller('\App\Http\Controllers\UserController')
            ->prefix('/user')->name('user.')->group( function()
    {
        Route::get('manage', 'index')->name('manage');
        Route::get('setup', 'show')->name('setup');
        Route::get('profile', 'profile')->name('profile');
        Route::get('edit', 'edit')->name('edit');
        Route::put('password-update', 'password_update')->name('password');
    });

    //..--* Class
    Route::controller('\App\Http\Controllers\ManageClassController')
            ->prefix('/class')->name('class.')->group( function()
    {
        Route::get('manage', 'index')->name('manage');
        Route::get('setup', 'show')->name('setup');
        Route::get('name', 'index_name')->name('name');
        Route::get('arm', 'arm')->name('arm');
    });

    //..--* Time Table
    Route::controller('\App\Http\Controllers\ManageTimeTableController')
            ->prefix('/time-table')->name('time-table.')->group( function()
    {
        Route::get('manage', 'index')->name('manage');
        Route::get('setup', 'show')->name('setup');
        Route::get('create', 'create')->name('create');
        Route::get('arm', 'arm')->name('arm');
    });
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
