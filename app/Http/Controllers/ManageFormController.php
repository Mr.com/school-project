<?php

namespace App\Http\Controllers;

use App\Models\ManageForm;
use Illuminate\Http\Request;

class ManageFormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pages.manageForm.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $fields = [['name' => 'Full Name', 'type' => 'input'],
                    ['name' => 'Full Name', 'type' => 'select']];
        return view('pages.manageForm.create', [
            'fields'     => $fields
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManageForm $manageForm)
    {
        return view('pages.manageForm.setup');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManageForm $manageForm)
    {
        return view('pages.manageForm.edit');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ManageForm $manageForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManageForm $manageForm)
    {
        return view('pages.manageForm.index');
    }

}
