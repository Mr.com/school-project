<?php

namespace App\Http\Controllers;

use App\Models\ManageTimeTable;
use App\Models\TimeTableLayoutOne;
use Illuminate\Http\Request;

class ManageTimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pages.manageTimeTable.edit');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $layoutOneRows = TimeTableLayoutOne::groupBy('row_id')
                                            ->get();
        $layoutOneColumns = TimeTableLayoutOne::groupBy('column_id')
                                                ->get();
        $tableSubjects  = TimeTableLayoutOne::all();
        
        return view('pages.manageTimeTable.create', [
            'layoutOneRows' => $layoutOneRows,
            'layoutOneColumns' => $layoutOneColumns,
            'tableSubjects' => $tableSubjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManageTimeTable $manageTimeTable)
    {
        return view('pages.manageTimeTable.setup');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManageTimeTable $manageTimeTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ManageTimeTable $manageTimeTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManageTimeTable $manageTimeTable)
    {
        //
    }
}
