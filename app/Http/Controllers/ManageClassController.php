<?php

namespace App\Http\Controllers;

use App\Models\ManageClass;
use App\Models\ClassName;
use Illuminate\Http\Request;

class ManageClassController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManageClass $manageClass)
    {
        return view('pages.manageClass.setup');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManageClass $manageClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ManageClass $manageClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManageClass $manageClass)
    {
        //
    }

    public function index_name()
    {
        return view('pages.manageClass.name');
    }

    public function store_name(Request $request)
    {
        
    }

    public function update_name(Request $request, ClassName $className)
    {

    }

    public function delete_name()
    {

    }

    public function restore_name()
    {

    }
}
