<?php

namespace App\Http\Controllers;

use App\Models\ManageStudent;
use Illuminate\Http\Request;

class ManageStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pages.manageStudent.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ManageStudent $manageStudent)
    {
        return view('pages.manageStudent.setup');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ManageStudent $manageStudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ManageStudent $manageStudent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ManageStudent $manageStudent)
    {
        //
    }
}
